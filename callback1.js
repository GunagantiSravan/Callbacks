function callback1 (id, data, cb){
    setTimeout(() => {
        let result = data.find(element => element.id === id);
        let err = `Error: "Data Not Found"`;
        result ? cb(null,result) : cb(err);
    }, 2 * 1000);
}

module.exports = callback1;
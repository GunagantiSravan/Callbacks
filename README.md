**Callbacks**

**Each function that you write must take at least 2s to execute using the setTimeout function like so:**

```
	function() {
		setTimeout(() => {
			// Your code here
		}, 2 * 1000);
	}


    How the given data is associated:
        Boards have ids and names
        Lists belong to boards
        Cards belong to lists


    Folder structure:
        ├── callback1.js
        ├── callback2.js
        ├── callback3.js
        ├── callback4.js
        ├── callback5.js
        ├── callback6.js
        └── test
            ├── testCallback1.js
            ├── testCallback2.js
            ├── testCallback3.js
            ├── testCallback4.js
            ├── testCallback5.js
            └── testCallback6.js

```
**Problem 1: Write a function that will return a particular board from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.**


**Problem 2: Write a function that will return a particular list that belongs to a board from the given data in lists.json and then pass control back to the code that called it by using a callback function.**


**Problem 3: Write a function that will return a particular set of cards that belong to a list from the given data in cards.json and then pass control back to the code that called it by using a callback function.**



**Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
    board -> lists -> cards for list qwsa221**


**Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
    board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously**


**Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
    board -> lists -> cards for all lists simultaneously**

